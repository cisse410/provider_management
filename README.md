# Flutter State Management with Provider 

Ce depot est un cours sur la gestion des etats en flutter avec Provider



## Etapes a suivre pour bien demarre le projet

Vous pouvez suivre succintement ces differentes etapes pour bien demarre l'application

## Cloner le projet avec la commande 

```
git clone https://gitlab.com/cisse410/provider_management.git
```

Vous pouvez aussi recuperer le fichier zip et le decompresse 

## Ouvrir le projet avec votre IDE de travail


## Recuperer les dependances avec la commande:

```
flutter pub get
```


## Selectionner un emulateur et demarre votre application

#3EN1 #L2F 🚀🚀🚀🚀


